package com.smile.index

import android.content.Context
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.smile.index.model.IndexDatabase
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType
import com.smile.index.model.dao.MediaDao
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MediaDaoTest {

    private var database: IndexDatabase? = null
    private var mediaDao: MediaDao? = null

    @Before
    fun createDB() {
        val context: Context = InstrumentationRegistry.getTargetContext()
        IndexDatabase.isTest = true
        database = IndexDatabase.getDatabase(context)
        mediaDao = database!!.getMediaDao()
    }

    @After
    fun closeDb() {
        IndexDatabase.destroyDatabase()
    }

    @Test
    fun insertMedia() {
        var media = Media(MediaType.Book, "test")
        var result = mediaDao!!.insert(media)
        Assert.assertEquals(result, 1)
    }

}