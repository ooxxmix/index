package com.smile.index.exception

class ISBNFormatException : Exception("ISBN Format Error")