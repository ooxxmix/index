package com.smile.index

import org.jsoup.Jsoup
import com.smile.index.exception.ISBNFormatException
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType
import java.io.IOException
import java.net.URL
import java.util.*

class EsliteSearcher : Searcher {

    private val timeoutMillis = 20000
    override val sourceName = "誠品"
    private val address = "http://www.eslite.com/Search_BW.aspx?query=%s"

    @Throws(ISBNFormatException::class, IOException::class)
    override fun search(isbn: String): Media {
        val url = URL(String.format(address, isbn))
        val doc = Jsoup.parse(url, timeoutMillis)
        val coverElement = doc.select("img[class=cover_img]")
        val titleElement = doc.select("td[class=name]")
        val detailElement = doc.select("td[class=summary]")
        val type = detailElement.select("span[id=ctl00_ContentPlaceHolder1_rptProducts_ctl00_LblCate]").select("a")[0]
            .text()
        val media = Media(getMediaType(type), titleElement.select("a[target=_blank]").attr("title"))
        media.isbn = isbn
        media.creator =
            detailElement.select("span[id=ctl00_ContentPlaceHolder1_rptProducts_ctl00_LblCharacterName]").select("a")
                .text()
        media.image = coverElement.attr("src")
        media.publish =
            detailElement.select("span[id=ctl00_ContentPlaceHolder1_rptProducts_ctl00_LblManufacturerName]").select("a")
                .text()
        media.note = titleElement.select("a[target=_blank]").attr("href")
        media.create = Date()
        media.update = Date()
        return media
    }

    override fun getMediaType(type: String): MediaType {
        if (type == "DVD")
            return MediaType.DVD
        else if (type == "CD")
            return MediaType.CD
        return MediaType.Book
    }

}