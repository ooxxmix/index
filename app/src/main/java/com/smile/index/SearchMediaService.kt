package com.smile.index

import android.app.IntentService
import android.content.Intent
import android.content.Context
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.smile.index.model.IndexDatabase

private const val SEARCHER = "com.smile.index.extra.SEARCHER"
private const val SEARCH_WORD = "com.smile.index.extra.SEARCH_WORD"

class SearchMediaService : IntentService("SearchMediaService") {

    override fun onHandleIntent(intent: Intent?) {

        search(intent!!.getSerializableExtra(SEARCHER) as Searcher, intent.getStringExtra(SEARCH_WORD))
    }

    private fun search(search: Searcher, searchWord: String) {
        Log.d("=TEST=", searchWord)
        val intent = Intent(SEARCH_FINISH)
        try {
            var book = search.search(searchWord)

            IndexDatabase.getDatabase(this)!!.getMediaDao().insert(book)

            intent.putExtra(SEARCH_RESULT, book.name)
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        } catch (e: Exception) {
            try {
                var book = EsliteSearcher().search(searchWord)
                IndexDatabase.getDatabase(this)!!.getMediaDao().insert(book)

                intent.putExtra(SEARCH_RESULT, book.name)
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            } catch (e: Exception) {
                e.printStackTrace()
                intent.putExtra(SEARCH_RESULT, e.toString())
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }
        }
    }

    companion object {

        const val SEARCH_FINISH = "com.smile.index.extra.SEARCH_FINISH"
        const val SEARCH_RESULT = "com.smile.index.extra.SEARCH_RESULT"

        @JvmStatic
        fun search(context: Context, searcher: Searcher, word: String) {
            val intent = Intent(context, SearchMediaService::class.java).apply {
                putExtra(SEARCHER, searcher)
                putExtra(SEARCH_WORD, word)
            }
            context.startService(intent)
        }
    }
}
