package com.smile.index

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.smile.index.model.bean.Media
import com.smile.index.presenter.MainPresenter
import com.smile.index.view.CameraDialog
import com.smile.index.view.IMainView
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.camera.*
import kotlinx.android.synthetic.main.content_main.*
import android.content.pm.PackageManager
import com.google.firebase.FirebaseApp
import android.content.IntentFilter
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.Index
import com.smile.index.model.IndexDatabase

class MainActivity : AppCompatActivity()
    , IMainView, View.OnClickListener, SearchView.OnQueryTextListener//, NavigationView.OnNavigationItemSelectedListener
{

    private val adapter = MediaAdapter()
    private var presenter: MainPresenter? = null
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateRecyclerView(IndexDatabase.getDatabase(context!!)!!.getMediaDao().get())
            content_main_recycler_view.scrollToPosition(adapter.media.size-1)
            closeCameraDialog()
        }
    }

    private val linearLayoutManager = LinearLayoutManager(this)
    private val gridLinearLayoutManager = GridLayoutManager(this, 2)

    private var menu: Menu? = null

    private var dialog: CameraDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setRecyclerView(linearLayoutManager, adapter)

        fab.setOnClickListener(this)

        presenter = MainPresenter(this, this)

        FirebaseApp.initializeApp(this)

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(receiver, IntentFilter(SearchMediaService.SEARCH_FINISH))

//        val toggle = ActionBarDrawerToggle(
//            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
//        )
//        drawer_layout.addDrawerListener(toggle)
//        toggle.syncState()
//
//        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        closeCameraDialog()
    }

//    override fun onBackPressed() {
//        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
//            drawer_layout.closeDrawer(GravityCompat.START)
//        } else {
//            super.onBackPressed()
//        }
//    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        this.menu = menu
        (menu.findItem(R.id.action_filter).actionView as SearchView).setOnQueryTextListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter -> true
            R.id.action_media_layout -> {
                if (content_main_recycler_view.layoutManager!!::class == LinearLayoutManager::class) {
                    setRecyclerView(gridLinearLayoutManager, adapter)
                    setRecyclerViewIcon(R.drawable.view_list)
                } else {
                    setRecyclerView(linearLayoutManager, adapter)
                    setRecyclerViewIcon(R.drawable.view_module)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

//    override fun onNavigationItemSelected(item: MenuItem): Boolean {
//        // Handle navigation view item clicks here.
//        when (item.itemId) {
//            R.id.nav_camera -> {
//                // Handle the camera action
//            }
//            R.id.nav_gallery -> {
//
//            }
//            R.id.nav_slideshow -> {
//
//            }
//            R.id.nav_manage -> {
//
//            }
//            R.id.nav_share -> {
//
//            }
//            R.id.nav_send -> {
//
//            }
//        }
//
//        drawer_layout.closeDrawer(GravityCompat.START)
//        return true
//    }true

    override fun onClick(v: View?) {
        when (v!!.id) {
            fab.id -> openCameraDialog()
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        adapter.filter.filter(newText)
        return false
    }

    override fun closeCameraDialog() {
        if (dialog != null)
            dialog!!.cancel()
    }

    override fun openCameraDialog() {
        if (dialog == null)
            dialog = CameraDialog(this)

        if (dialog!!.isShowing)
            closeCameraDialog()

        dialog!!.show()
    }

    override fun updateRecyclerView(data: List<Media>) {
        adapter.updateData(data)
    }

    override fun setRecyclerView(layoutManager: LinearLayoutManager, adapter: MediaAdapter) {
        content_main_recycler_view.setHasFixedSize(true)
        content_main_recycler_view.layoutManager = layoutManager
        content_main_recycler_view.adapter = adapter
        adapter.listener = object : OnRecyclerViewClickListener {
            override fun onItemClickListener(view: View?) {
            }

            override fun onItemLongClickListener(view: View?) {
                var position = content_main_recycler_view.getChildAdapterPosition(view!!)
                var media = adapter.media[position]
                Log.d("=TEST=", media.name)
                IndexDatabase.getDatabase(this@MainActivity)!!.getMediaDao().delete(media)
                updateRecyclerView(IndexDatabase.getDatabase(this@MainActivity)!!.getMediaDao().get())
            }
        }
    }

    override fun setRecyclerViewIcon(drawableId: Int) {
        menu!!.findItem(R.id.action_media_layout).setIcon(drawableId)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var valid = true
        for (grantResult in grantResults) {
            valid = valid && grantResult == PackageManager.PERMISSION_GRANTED
        }
        if (valid && !camera.isOpened) {
            openCameraDialog()
        }
    }
}
