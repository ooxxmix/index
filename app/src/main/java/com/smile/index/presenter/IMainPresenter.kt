package com.smile.index.presenter

import com.smile.index.model.bean.Media

interface IMainPresenter {

    fun addMedia(media: Media)

    fun removeMedia(media: Media)

}