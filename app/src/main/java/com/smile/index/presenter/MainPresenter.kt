package com.smile.index.presenter

import android.content.Context
import com.smile.index.model.IndexDatabase
import com.smile.index.model.bean.Media
import com.smile.index.view.IMainView

class MainPresenter(context: Context, val view: IMainView) : IMainPresenter {

    private val database: IndexDatabase = IndexDatabase.getDatabase(context)!!
    private val dao = database.getMediaDao()

    init {
        view.updateRecyclerView(dao.get() as ArrayList<Media>)
    }

    override fun addMedia(media: Media) {
        dao.insert(media)
        view.updateRecyclerView(dao.get() as ArrayList<Media>)
    }

    override fun removeMedia(media: Media) {
        dao.delete(media)
        view.updateRecyclerView(dao.get() as ArrayList<Media>)
    }

}