package com.smile.index

import com.smile.index.exception.ISBNFormatException
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType
import org.jsoup.Jsoup
import java.io.IOException
import java.net.URL
import java.util.*

class BooksSearcher : Searcher {

    private val timeoutMillis = 20000
    override val sourceName = "博客來"
    private val address = "https://search.books.com.tw/search/query/key/%s"

    @Throws(ISBNFormatException::class, IOException::class)
    override fun search(isbn: String): Media {
        val url = URL(String.format(address, isbn))
        val doc = Jsoup.parse(url, timeoutMillis)
        val element = doc.select("li[class=item]")[0]
        val type = getMediaType(element.select("span[class=cat]").text())
        val media = Media(type, element.select("a[rel=mid_name]").text())
        media.isbn = isbn
        media.creator = element.select("a[rel=go_author]").text()
        media.image = element.select("img[class=itemcov]").attr("data-original")
        media.publish = element.select("a[rel=mid_publish]").text()
        val link = element.select("a[rel=mid_name]").attr("href")
        media.note = if (link.startsWith("https:")) link else "https:".plus(link)
        media.create = Date()
        media.update = Date()
        return media
    }

    override fun getMediaType(type: String): MediaType {
        if (type == "DVD")
            return MediaType.DVD
        else if (type == "CD")
            return MediaType.CD
        return MediaType.Book
    }

}
