package com.smile.index

import android.app.Application
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller

class IndexApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        try {
            ProviderInstaller.installIfNeeded(this)
        } catch (e: GooglePlayServicesRepairableException) {

        }

    }
}
