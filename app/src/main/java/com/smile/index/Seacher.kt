package com.smile.index

import com.smile.index.exception.ISBNFormatException
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType
import java.io.IOException
import java.io.Serializable


interface Searcher : Serializable {

    val sourceName: String

    @Throws(ISBNFormatException::class, IOException::class)
    fun search(isbn: String): Media

    fun getMediaType(type: String): MediaType

//    companion object {
//        fun getMedia(isbn: String, name: String): Media {
//            return Book(name)
//        }
//    }
}