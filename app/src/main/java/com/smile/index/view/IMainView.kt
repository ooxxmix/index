package com.smile.index.view

import androidx.recyclerview.widget.LinearLayoutManager
import com.smile.index.MediaAdapter
import com.smile.index.model.bean.Media

interface IMainView {

    fun openCameraDialog()

    fun closeCameraDialog()

    fun updateRecyclerView(data: List<Media>)

    fun setRecyclerView(layoutManager: LinearLayoutManager, adapter: MediaAdapter)

    fun setRecyclerViewIcon(drawableId: Int)

}