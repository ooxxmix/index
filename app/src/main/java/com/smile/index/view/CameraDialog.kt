package com.smile.index.view

import android.app.Dialog
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import kotlinx.android.synthetic.main.camera.*
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata
import com.otaliastudios.cameraview.*
import com.smile.index.*
import com.smile.index.R
import java.lang.Exception

class CameraDialog(private val activity: AppCompatActivity) : Dialog(activity)
    , FrameProcessor, OnSuccessListener<List<FirebaseVisionBarcode>>, OnFailureListener
    , LifecycleOwner {

    private lateinit var lifecycleRegistry: LifecycleRegistry

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    var flag = true

    var options = FirebaseVisionBarcodeDetectorOptions.Builder()
        .setBarcodeFormats(
            FirebaseVisionBarcode.TYPE_ISBN
//            , FirebaseVisionBarcode.FORMAT_CODABAR
//            , FirebaseVisionBarcode.FORMAT_QR_CODE
            , FirebaseVisionBarcode.FORMAT_EAN_13
        )
        .build()
    var detector: FirebaseVisionBarcodeDetector = FirebaseVision.getInstance().getVisionBarcodeDetector(options)

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.camera)
        setCanceledOnTouchOutside(false)
        window.setDimAmount(0f)
        window.setGravity(Gravity.TOP)

        val tv = TypedValue()
        if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            val actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.resources.displayMetrics)
            val param = window.attributes
            param.y = (actionBarHeight * 1.5).toInt()
            window.attributes = param
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleRegistry = LifecycleRegistry(this)
        lifecycleRegistry.markState(Lifecycle.State.CREATED)
    }

    override fun onStart() {
        super.onStart()
        lifecycleRegistry.markState(Lifecycle.State.STARTED)

        camera.setLifecycleOwner(this)
        camera.mapGesture(Gesture.PINCH, GestureAction.ZOOM)
        camera.mapGesture(Gesture.TAP, GestureAction.FOCUS_WITH_MARKER)
        camera.addFrameProcessor(this)

//        camera.pictureSize.height
//        camera.rotation
//        camera.pictureSize.width
        flag = true
        camera.open()
    }

    override fun onStop() {
        super.onStop()
        lifecycleRegistry.markState(Lifecycle.State.DESTROYED)

        camera.clearCameraListeners()
        camera.clearFrameProcessors()
        camera.close()
        camera.destroy()
    }

    override fun process(frame: Frame) {
        val metadata = FirebaseVisionImageMetadata.Builder()
            .setWidth(frame.size.width)
            .setHeight(frame.size.height)
//            .setRotation(frame.rotation)
            .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_NV21)
            .build()
        val image = FirebaseVisionImage.fromByteArray(frame.data, metadata)

        detector.detectInImage(image)
        detector.detectInImage(image)
            .addOnSuccessListener(this)
            .addOnFailureListener(this)
    }

    override fun onSuccess(p: List<FirebaseVisionBarcode>) {
        for (barcode in p) {
            if (flag) {
                flag = false
                Toast.makeText(activity, barcode.rawValue, Toast.LENGTH_SHORT).show()
                SearchMediaService.search(context, BooksSearcher(), barcode.rawValue!!)
            }
        }
    }

    override fun onFailure(p: Exception) {
        p.printStackTrace()
    }

}
