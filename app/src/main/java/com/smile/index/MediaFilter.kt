package com.smile.index

import android.widget.Filter
import com.smile.index.model.bean.Media

class MediaFilter(private val adapter: MediaAdapter) : Filter() {

    override fun performFiltering(constraint: CharSequence?): FilterResults {
        val filteredList: ArrayList<Media> = adapter.media
        val charString = constraint.toString()
        if (charString.isNotEmpty()) {
            val filteredList = ArrayList<Media>()
            for (row in adapter.media) {
                if (row.name.toLowerCase().contains(charString.toLowerCase()))
                    filteredList.add(row)
            }
        }
        val filterResults = Filter.FilterResults()
        filterResults.values = if (filteredList.isEmpty()) adapter.media else filteredList
        return filterResults
    }

    override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
        adapter.updateData(results!!.values as List<Media>)
        adapter.notifyDataSetChanged()
    }

}