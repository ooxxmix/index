package com.smile.index

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.smile.index.model.bean.Media
import kotlin.collections.ArrayList

class MediaAdapter : RecyclerView.Adapter<MediaAdapter.ViewHolder>(), Filterable {

    val media: ArrayList<Media> = ArrayList()

    private var filter: MediaFilter = MediaFilter(this)

    var listener: OnRecyclerViewClickListener? = null

    override fun getFilter(): Filter = this.filter

    override fun getItemCount(): Int = media.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.media_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(media[position])
    }

    fun updateData(data: List<Media>) {
        media.clear()
        media.addAll(data)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
        , View.OnClickListener, View.OnLongClickListener {

        private val name = itemView.findViewById<TextView>(R.id.title)
        private val icon = itemView.findViewById<ImageView>(R.id.icon)

        fun bind(media: Media) {
            name.text = media.name
            Glide.with(itemView.context).load(media.image).into(icon)
            if (listener != null) {
                itemView.setOnClickListener(this)
                itemView.setOnLongClickListener(this)
            }
        }

        override fun onClick(v: View?) {
            listener!!.onItemClickListener(v)
        }

        override fun onLongClick(v: View?): Boolean {
            listener!!.onItemLongClickListener(v)
            return true
        }

    }
}

interface OnRecyclerViewClickListener {

    fun onItemClickListener(view: View?)

    fun onItemLongClickListener(view: View?)

}