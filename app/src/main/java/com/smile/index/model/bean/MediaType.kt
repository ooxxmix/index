package com.smile.index.model.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.smile.index.model.bean.MediaType.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
open class MediaType(
    @PrimaryKey
    val id: Int
    , val type: String
) : BaseEntity {

    companion object {
        const val TABLE_NAME = "media_type"

        val None = MediaType(0, "None")
        val Book = MediaType(1, "Book")
        val DVD = MediaType(2, "DVD")
        val CD = MediaType(3, "CD")
    }

    override fun getTableName(): String {
        return TABLE_NAME
    }

}