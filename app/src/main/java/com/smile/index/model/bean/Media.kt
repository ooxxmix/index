package com.smile.index.model.bean

import androidx.room.*
import androidx.annotation.NonNull
import com.smile.index.exception.ISBNFormatException
import com.smile.index.model.bean.Media.Companion.TABLE_NAME
import java.util.*
import java.util.regex.Pattern

//class Book @Throws(ISBNFormatException::class)
//constructor(name: String) : Media(MediaType.Book, name)
//
//class CD @Throws(ISBNFormatException::class)
//constructor(name: String) : Media(MediaType.CD, name)
//
//class DVD @Throws(ISBNFormatException::class)
//constructor(name: String) : Media(MediaType.DVD, name)

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [ForeignKey(
        entity = MediaType::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("typeId")
    )]
)
open class Media : BaseEntity {

    companion object {
        const val TABLE_NAME = "media"
    }

    override fun getTableName(): String {
        return TABLE_NAME
    }

    @Ignore
    private val regex =
        "^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$"

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @NonNull
    var typeId: Int

    @NonNull
    var name: String = ""

    var isbn: String = ""
    //    var creator: List<String>? = null
    var creator: String = ""
    var image: String = ""
    var publish: String = ""
    var note: String = ""

    @TypeConverters(DateConverter::class)
    var create: Date? = null
    @TypeConverters(DateConverter::class)
    var update: Date? = null

    constructor() {
        this.typeId = MediaType.None.id
    }

    @Throws(ISBNFormatException::class)
    constructor(type: MediaType, name: String) {
        this.typeId = type.id
        this.name = name
    }

//    @Throws(ISBNFormatException::class)
//    constructor(type: MediaType, name: String, isbn: String) : this(type, name) {
//        val pattern = Pattern.compile(regex)
//        if (!pattern.matcher(isbn).matches())
//            throw ISBNFormatException()
//        this.isbn = isbn
//    }

}
