package com.smile.index.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType

@Dao
interface MediaDao : BaseDao<Media> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addMediaType(obj: MediaType): Long

    @Query("select * from " + MediaType.TABLE_NAME)
    fun getMediaType(): List<MediaType>

    @Query("select * from " + Media.TABLE_NAME)
    fun get(): List<Media>

    @Query("select * from " + Media.TABLE_NAME + " where id = :id")
    fun get(id: Long): Media

//    @Query("select * from " + Media.TABLE_NAME + " where typeId = :MediaType.Book.id")
//    fun getBooks(): List<Media>

}