package com.smile.index.model.bean

import androidx.room.TypeConverter

class MediaTypeConverter {

    @TypeConverter
    fun fromMediaType(mediaType: MediaType): Int {
        return mediaType.id
    }

    @TypeConverter
    fun toMediaType(mediaType: MediaType): String {
        return mediaType.type
    }

}