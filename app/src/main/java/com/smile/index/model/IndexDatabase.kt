package com.smile.index.model

import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.smile.index.model.bean.Media
import com.smile.index.model.bean.MediaType
import com.smile.index.model.dao.MediaDao
import java.util.concurrent.Executors

@Database(entities = [Media::class, MediaType::class], version = 1)
abstract class IndexDatabase : RoomDatabase() {

    abstract fun getMediaDao(): MediaDao

    companion object {
        private const val DATABASE_NAME = "index"
        @Volatile
        private var INSTANCE: IndexDatabase? = null

        var isTest = false
        private val initCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                var executor = Executors.newSingleThreadExecutor()
                executor.execute {
                    var dao = INSTANCE!!.getMediaDao()
                    dao.addMediaType(MediaType.None)
                    dao.addMediaType(MediaType.Book)
                    dao.addMediaType(MediaType.CD)
                    dao.addMediaType(MediaType.DVD)
                    db.beginTransaction()
                    db.endTransaction()
                }
            }
        }

        fun getDatabase(context: Context): IndexDatabase? {
            if (INSTANCE == null) {
                synchronized(IndexDatabase::class) {
                    INSTANCE = if (isTest) {
                        Room.inMemoryDatabaseBuilder(context.applicationContext, IndexDatabase::class.java)
                            .addCallback(initCallback)
                            .build()
                    } else {
                        Room.databaseBuilder(context.applicationContext, IndexDatabase::class.java, DATABASE_NAME)
                            .addCallback(initCallback)
                            .allowMainThreadQueries()
                            .build()
                    }
                }
            }
            return INSTANCE
        }

        fun destroyDatabase() {
            INSTANCE = null
        }
    }
}