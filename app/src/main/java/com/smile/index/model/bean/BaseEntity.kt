package com.smile.index.model.bean

interface BaseEntity {

    fun getTableName(): String

}